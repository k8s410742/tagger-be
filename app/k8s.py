from kubernetes import client, config
from os import environ
import datetime

# find out if we are running in container or not
# container is for deployment and local is for testing
if environ.get("PROD") == "true":
    config.load_incluster_config()
else:
    config.load_kube_config()


class K8S:
    def __init__(self):
        self.core_api = client.CoreV1Api()
        self.apps_api = client.AppsV1Api()

    def get_image_tag(self, namespace) -> str:
        # get all deployments in namespace
        api_response = self.apps_api.list_namespaced_deployment(namespace)

        # loop through deployments and get tags
        tags = []

        for deployment in api_response.items:
            # get full image name
            image = deployment.spec.template.spec.containers[0].image
            # get tag
            tag = image.split(":")[-1]
            if not tag in tags:
                tags.append(tag)

        if len(tags) == 1:
            return tags[0]
        elif len(tags) == 0:
            print("ERROR: no tag found")
            return None
        else:
            print("ERROR: more than one tag found")
            return None

    def get_namespaces(self) -> list:
        # get all namespaces except kube-system
        ignore = [
            "kube-system",
            "tagger",
            "kube-public",
            "cilium-secrets",
            "kube-node-lease",
        ]  #! add more namespaces to ignore here
        namespaces = []
        api_response = self.core_api.list_namespace()
        for namespace in api_response.items:
            if not namespace.metadata.name in ignore:
                namespaces.append(namespace.metadata.name)
        # standarize the response to look like
        # [{"id": <id>, "name": <namespace-name>, "status": 1}, {...}]
        # id will just be a unique number for each namespace
        namespaces = [
            {"id": i, "name": namespace, "status": 1}
            for i, namespace in enumerate(namespaces)
        ]
        return namespaces

    def update_image_tag(self, namespace, tag):
        # check if the namespace is valid for this tag update
        namespace_tag = self.get_image_tag(namespace)
        if namespace_tag is None:
            print("This namespace is not compatible for this tag update")
            return None

        # get all deployments in namespace
        api_response = self.apps_api.list_namespaced_deployment(namespace)

        # loop through deployments and update their tags
        for deployment in api_response.items:
            # get full image name
            image = deployment.spec.template.spec.containers[0].image
            # get image name
            image_name = image.split(":")[0]
            # update image tag
            new_image = image_name + ":" + tag
            # update deployment
            deployment.spec.template.spec.containers[0].image = new_image
            # update deployment
            api_response = self.apps_api.patch_namespaced_deployment(
                deployment.metadata.name, namespace, deployment
            )
            print("Deployment updated")

    def restart_deployments(self, namespace):
        # patch all deployments in namespace to trigger a rolling restart
        api_response = self.apps_api.list_namespaced_deployment(namespace)
        for deployment in api_response.items:
            # get current timestamp
            now = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")
            # update deployment
            api_response = self.apps_api.patch_namespaced_deployment(
                deployment.metadata.name,
                namespace,
                {
                    "spec": {
                        "template": {
                            "metadata": {
                                "annotations": {
                                    "kubectl.kubernetes.io/restartedAt": now
                                }
                            }
                        }
                    }
                },
            )
            print("Deployment restarted")


if __name__ == "__main__":
    k8s = K8S()
    print(k8s.get_namespaces())
