from fastapi import FastAPI, Form, status
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from k8s import K8S

app = FastAPI()
k8s = K8S()


@app.get("/get-namespaces")
def get_namespaces() -> JSONResponse:
    namespaces = k8s.get_namespaces()
    response = JSONResponse(content=jsonable_encoder(namespaces))
    return response


@app.get("/get-image-tag/{namespace}")
def get_image_tag(namespace: str) -> JSONResponse:
    tag = k8s.get_image_tag(namespace)
    response = JSONResponse(content=jsonable_encoder(tag))
    return response


@app.post("/update-image-tag")
def update_image_tag(namespace: str = Form(...), tag: str = Form(...)) -> JSONResponse:
    k8s.update_image_tag(namespace, tag)
    response = JSONResponse(content=jsonable_encoder("OK"))
    return response


@app.post("/restart-deployments")
def restart_deployments(namespace: str = Form(...)) -> JSONResponse:
    k8s.restart_deployments(namespace)
    response = JSONResponse(content=jsonable_encoder("OK"))
    return response


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=5000)
